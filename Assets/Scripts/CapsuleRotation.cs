﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    [SerializeField]
    private Vector3 RotateAxis; //Ejes de Rotación
    public float RotateSpeed; //Vel. de rotacion

    // Update is called once per frame
    void Update()
    {
        //Acotación de vectores de rotación [-1,1]
        RotateAxis = ClampVector3(RotateAxis);

        //Vel.*Tiempo=Distancia
        //Distancia*Vector=Dirección y módulo
        transform.Rotate(RotateAxis * (RotateSpeed * Time.deltaTime));
    }
        //Función auxiliar para acotar vectores de los componentes entre -1 y 1
        public static Vector3 ClampVector3(Vector3 target)
{
    float clampedX = Mathf.Clamp(target.x, -1f, 1f);
    float clampedY = Mathf.Clamp(target.y, -1f, 1f);
    float clampedZ = Mathf.Clamp(target.z, -1f, 1f);

    Vector3 result = new Vector3(clampedX, clampedY, clampedZ);

    return result;
}




}
