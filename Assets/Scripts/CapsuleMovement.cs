﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour
{
    [SerializeField]
    private Vector3 MoveAxis; //Ejes de movimiento
    public float MoveSpeed;         //Velocidad

    // Llamado cada frame
    void Update()
    {
        //Acotacion de vectores de direccion al valor unitario , o sea entre -1 y 1
        MoveAxis = ClampVector3(MoveAxis);

        //Recuerda que Velocidad*Tiempo=Distancia.
        //Distancia*Dirección= Distancia recorrida en la dirección.
        transform.Translate(MoveAxis * (MoveSpeed * Time.deltaTime));
    }
    
     // Función auxiliar para acotar vectores de las componentes entre -1 y 1
     public static Vector3 ClampVector3(Vector3 target) 
        {
            float clampedX = Mathf.Clamp(target.x, -1f, 1f);
            float clampedY = Mathf.Clamp(target.y, -1f, 1f);
            float clampedZ = Mathf.Clamp(target.z, -1f, 1f);


        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);

            return result;
        }
    
}
