﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 scaleAxis;       //ejes de escalado
    public float scaleUnits;    //vel. de escalado

    // Update is called once per frame
    void Update()
    {
        //Acotacion de valores de escalado al valor unitario [-1,1]
        scaleAxis = CapsuleMovement.ClampVector3(scaleAxis);

        /*
         * La escala es acumulativa, a diferencia de la rotacion y movimiento.
         * Debemos añadir el nuevo valor d ela escala al valor anterior. 
         */
        transform.localScale += scaleAxis * (scaleUnits * Time.deltaTime);
    }
    
}
